# MyFirstApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.


## What it does

Created a based version using ng new
Modified the html file with a place holder to eneter name

## Steps Done
* Added a variable - name in the component
* Component HTML is modified by attaching the name attribute to the input text element using [{ngModel}]
* Render the user enetered text in the browser using {{name}}
* Adding a module FormsModule in the Import section of app.modules

## Execution
Use ng serve

# Adding Bootstrap to the project

 * Add bootstrap to project using npm install --save bootstrap
 * we need to now refer the bootstrap.style in the project. Modify the *angular.json* file under styles section. Add the path "node_modules/bootstrap/dist/css/bootstrap.min.css",
* restart the ng server to see bootstrap is added to the index.html

 